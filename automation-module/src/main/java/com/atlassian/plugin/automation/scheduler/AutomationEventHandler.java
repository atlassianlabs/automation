package com.atlassian.plugin.automation.scheduler;

/**
 * Unified way of handling event handlers, basically a tagging interface. atlassian-events only needs Object to register
 */
public interface AutomationEventHandler
{
    @SuppressWarnings("unused")
    public void handleEvent(final Object event);
}
