package com.atlassian.plugin.automation.upgrade;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;

/**
 * We added an option to toggle synchronous events-based rule processing, so this task sets the default to false
 *
 * @since v5.0
 */
@Named
@ExportAsService
public class UpgradeSettingsWithSynchronousThreads implements PluginUpgradeTask
{
    private static final Logger log = Logger.getLogger(UpgradeSettingsWithSynchronousThreads.class);
    private final PluginSettings pluginSettings;

    @Inject
    public UpgradeSettingsWithSynchronousThreads (@ComponentImport final PluginSettingsFactory pluginSettingsFactory)
    {
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public int getBuildNumber()
    {
        return 5;
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrades settings with default values for synchronous threads settings.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        pluginSettings.put(Settings.SYNC_EVENT_HANDLING_ENABLED, String.valueOf(false));
        return Collections.emptyList();
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.plugin.automation.automation-module";
    }
}