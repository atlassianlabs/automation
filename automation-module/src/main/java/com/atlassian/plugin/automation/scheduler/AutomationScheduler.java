package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Rule;

/**
 * This interface provides unified way of entering tasks to automation queue
 */
public interface AutomationScheduler
{
    /**
     * Schedules given rule according to it's trigger
     * @param rule
     */
    void scheduleRule(final Rule rule);

    /**
     * Unschedule rule once it's removed
     * @param ruleId
     */
    void unscheduleRule(int ruleId);

    /**
     * Unschedules all rules.
     */
    void unscheduleAllRules();
}
