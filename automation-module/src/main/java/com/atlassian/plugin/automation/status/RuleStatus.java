package com.atlassian.plugin.automation.status;

import java.io.Serializable;

public enum RuleStatus implements Serializable
{
    OK(1),
    FAILED(2),
    UNKNOWN(999);

    protected int code;

    // For serialization
    RuleStatus() {}

    private RuleStatus(int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return code;
    }
}
