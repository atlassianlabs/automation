package com.atlassian.plugin.automation.scheduler;

import com.atlassian.cache.compat.Cache;
import com.atlassian.cache.compat.CacheFactory;
import com.atlassian.cache.compat.CacheLoader;
import com.atlassian.cache.compat.CacheSettingsBuilder;
import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.auditlog.EventAuditMessageBuilder;
import com.atlassian.plugin.automation.auditlog.EventAuditLogService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.concurrent.TimeUnit;

/**
 * Limits the execution of the rules by checking how many times certain rule was executed within given time frame
 */
@Named
@SuppressWarnings("unused")
public class DefaultRuleExecutionLimiter implements RuleExecutionLimiter
{
    private static final Logger log = Logger.getLogger(DefaultRuleExecutionLimiter.class);
    private static final int MIN_TIME_BETWEEN_EXECUTIONS_MS = 500;
    private static final int MAX_ACCESS_COUNT = 20;

    private final Cache<Integer, AccessTracker> ruleExecutions;

    private final EventAuditLogService eventAuditLogService;
    private final PluginSettings pluginSettings;

    @Inject
    public DefaultRuleExecutionLimiter(
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            final EventAuditLogService eventAuditLogService,
            final CacheFactory cacheFactory)
    {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
        this.eventAuditLogService = eventAuditLogService;

        // we should not need any max entries, as this is only going to contain as many entries as there are rules
        this.ruleExecutions = cacheFactory.getCache(this.getClass().getName(),
                new CacheLoader<Integer, AccessTracker>()
                {
                    @Override
                    public AccessTracker load(@Nonnull Integer key)
                    {
                        return new AccessTracker();
                    }
                },
                new CacheSettingsBuilder()
                        .expireAfterAccess(5, TimeUnit.MINUTES)
                        .local()    // this is okay as the possible loop should only occur locally anyway
                        .flushable()
                        .build()
        );
    }

    @Override
    public boolean canExecuteActions(final Rule rule, String triggerString)
    {
        final boolean isLimiterEnabled = Boolean.valueOf((String) pluginSettings.get(Settings.LIMITER_ENABLED));

        if (isLimiterEnabled)
        {
            AccessTracker accessTracker = ruleExecutions.get(rule.getId());
            final int accessCount = accessTracker.access();
            if (accessCount > MAX_ACCESS_COUNT)
            {
                final String message = String.format("Possible loop detected! Rule '%s' was executed more than '%d' times in last '%d' ms.",
                        rule.getName(), accessCount - 1, MIN_TIME_BETWEEN_EXECUTIONS_MS);
                log.error(message);
                eventAuditLogService.addEntry(new EventAuditMessageBuilder().setMessage("Auto-disabling rule").
                        setErrors(message).setTriggerMessage(triggerString).setRuleId(rule.getId()).setActor(rule.getActor()).build());
                return false;
            }
        }
        return true;
    }

    static class AccessTracker
    {
        long lastTimestamp;
        int accessCount;

        AccessTracker()
        {
            this.lastTimestamp = System.currentTimeMillis();
            accessCount = 0;
        }

        int access()
        {
            final long currrentTime = System.currentTimeMillis();
            final long difference = Math.abs(currrentTime - lastTimestamp);
            lastTimestamp = currrentTime;
            if (difference < MIN_TIME_BETWEEN_EXECUTIONS_MS)
            {
                accessCount++;
            }
            else
            {
                accessCount = 1;
            }
            return accessCount;
        }
    }
}
