package com.atlassian.plugin.automation.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import net.java.ao.DBParam;
import net.java.ao.Query;

import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;
import javax.inject.Named;

import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionConfigEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionConfigValueEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.TriggerConfigEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.TriggerConfigValueEntity;

/**
 * Moves parameters from ConfigEntity to
 */
@Named
@ExportAsService
public class UpgradeAutomationParams implements PluginUpgradeTask
{
    private final ActiveObjects ao;

    @Inject
    public UpgradeAutomationParams(@ComponentImport final ActiveObjects ao)
    {
        this.ao = ao;
    }

    @Override
    public int getBuildNumber()
    {
        return 3;
    }

    @Override
    public String getShortDescription()
    {
        return "Converting automation params to support multi-value parameters.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        final ActionConfigEntity[] actionEntities = ao.find(ActionConfigEntity.class, Query.select().where("PARAM_VALUE IS NOT NULL"));
        for (ActionConfigEntity configEntity : actionEntities)
        {
            final String paramValue = configEntity.getParamValue();
            ao.create(ActionConfigValueEntity.class, new DBParam(ActionConfigEntity.ACTION_CONFIG_ID, configEntity.getID()), new DBParam("PARAM_VALUE", paramValue));
            configEntity.setParamValue(null);
            configEntity.save();
        }

        final TriggerConfigEntity[] triggerEntities = ao.find(TriggerConfigEntity.class, Query.select().where("PARAM_VALUE IS NOT NULL"));
        for (TriggerConfigEntity configEntity : triggerEntities)
        {
            final String paramValue = configEntity.getParamValue();
            ao.create(TriggerConfigValueEntity.class, new DBParam(TriggerConfigEntity.TRIGGER_CONFIG_ID, configEntity.getID()), new DBParam("PARAM_VALUE", paramValue));
            configEntity.setParamValue(null);
            configEntity.save();
        }
        return Collections.emptyList();
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.plugin.automation.automation-module";
    }
}
