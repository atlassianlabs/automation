package com.atlassian.plugin.automation.admin;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.scheduler.SettingsAwareRuleLogService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Scanned
public class AutomationConfigServlet extends AbstractAdminServlet
{
    private static final String ADMIN_RESOURCES = COMPLETE_PLUGIN_KEY + ":automation-module-config-client-resources";
    private final PluginSettings pluginSettings;
    private final SettingsAwareRuleLogService ruleLogService;

    @Inject
    public AutomationConfigServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager,
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            final SettingsAwareRuleLogService ruleLogService)
    {
        super(webSudoManager, renderer, userManager, loginUriProvider, webResourceManager);
        this.ruleLogService = ruleLogService;
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    protected void requireResource(WebResourceManager webResourceManager)
    {
        webResourceManager.requireResource(ADMIN_RESOURCES);
    }

    @Override
    protected void renderResponse(SoyTemplateRenderer renderer, HttpServletRequest request, HttpServletResponse response) throws IOException, SoyException
    {
        final Map<String, Object> context = newHashMap();
        context.put("limiterEnabled", Boolean.valueOf((String)pluginSettings.get(Settings.LIMITER_ENABLED)));
        context.put("ruleLogEnabled", Boolean.valueOf((String) pluginSettings.get(Settings.PRODUCT_RULE_LOG_ENABLED)));
        if (ruleLogService.isAvailable())
        {
            context.put("ruleLogAvailable", "true");
        }
        context.put("syncEventHandlingEnabled", Boolean.valueOf((String)pluginSettings.get(Settings.SYNC_EVENT_HANDLING_ENABLED)));

        renderer.render(response.getWriter(), CONFIG_RESOURCE_KEY, "Atlassian.Templates.Automation.automationConfig", context);
    }
}