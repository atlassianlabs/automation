package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.spi.auditlog.RuleLogService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.inject.Inject;
import javax.inject.Named;

/*
 * This class represents rule log service implemented in the SPI for each product which supports it.
 */
@Named
public class SettingsAwareRuleLogService
{
    private final RuleLogService ruleLogService;
    private final PluginSettings pluginSettings;

    @Inject
    public SettingsAwareRuleLogService(@ComponentImport final RuleLogService ruleLogService,
                                       @ComponentImport final PluginSettingsFactory pluginSettingsFactory)
    {
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
        this.ruleLogService = ruleLogService;
    }

    public boolean isEnabled()
    {
        return Boolean.valueOf((String) pluginSettings.get(Settings.PRODUCT_RULE_LOG_ENABLED));
    }

    public void logRuleExecution(Rule rule, Object associatedItem)
    {
        if (isEnabled())
        {
            ruleLogService.logRuleExecution(rule, associatedItem);
        }
    }

    public boolean isAvailable()
    {
        return ruleLogService.isAvailable();
    }
}
