AJS.namespace("Atlassian.Automation.ActionFormView");
AJS.namespace("Atlassian.Automation.SingleActionView");

Atlassian.Automation.ActionFormView = Brace.View.extend({
    template: Atlassian.Templates.Automation.actionsForm,

    events: {
        "click #add-action-button": "addAction"
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'render', 'updateModel', 'addAction', 'displayErrors', 'destroy');

        this.model.get("rule").on("change", this.render);
        this.model.on("saveFormState", this.updateModel);
        this.model.on("change:errors", this.displayErrors);

        this.actionsCollection = this.model.get("rule").get("actions");

        this.actionsCollection.on("add", this.addOne);
        this.actionViewCount = 0;
    },

    addAction: function (e) {
        e.preventDefault();
        this.actionsCollection.add(new Atlassian.Automation.ActionModel());
    },

    addOne: function (actionModel) {
        var singleActionView = new Atlassian.Automation.SingleActionView({
            model: actionModel,
            configModel: this.model,
            actionCount: this.actionViewCount++
        });
        this.$("#actions-container").append(singleActionView.render());
    },

    render: function () {
        this.$el.html(this.template({
            id:this.model.get("rule").get("id")
        }));

        var that = this;
        this.actionsCollection.each(function (actionModel) {
            that.addOne(actionModel);
        });

        // Add first one
        if (this.actionsCollection.length == 0) {
            this.actionsCollection.add(new Atlassian.Automation.ActionModel());
        }

        return this;
    },

    updateModel: function () {
        //propagate the event on the actions collection so that all the single action views will save their
        //form state.
        this.model.get("rule").get("actions").each(function (actionModel) {
            actionModel.trigger("saveFormState");
        });
    },

    displayErrors: function () {
        //clear all errors
        this.$("form.automation-action-form .error").remove();

        //this is a bit shite. It should really be the responsibility of the SingleActionView to
        //render these.
        var errorCollections = this.model.get("errors");
        if (typeof errorCollections !== "undefined") {
            var $forms = this.$("form.automation-action-form");
            var count = 0;
            var actionErrors = errorCollections.actionErrors;
            $forms.each(function (i, $form) {
                var errorCollection = actionErrors[i];
                AJS.$.each(errorCollection.errors, function (field, message) {
                    AJS.$("[name=" + field + "]", $form).after(Atlassian.Templates.Automation.errorDiv({message: message}));
                });
                count++;
            });
        }
    },

    destroy: function () {
        //clean up events
        this.model.get("rule").off("change", this.render);
        this.model.off("saveFormState", this.updateModel);
        this.model.off("change:errors", this.displayErrors);
        this.undelegateEvents();
    }

});

Atlassian.Automation.SingleActionView = Brace.View.extend({
    template: Atlassian.Templates.Automation.singleActionForm,
    tagName: "form",
    className: "aui automation-action-form ajs-dirty-warning-exempt",

    events: {
        "change .action-module-key": "actionChanged",
        "click .delete-action-button": "removeAction"
    },

    initialize: function () {
        _.bindAll(this, 'actionChanged', 'render', 'updateModel', 'removeAction', 'toggleDeleteButton');

        this.$el.attr("id", "action-form-" + this.options.actionCount);
        this.model.on("saveFormState", this.updateModel);


        this.actionModules = this.options.configModel.get("actionModules");
        this.actions = this.options.configModel.get("rule").get("actions");

        this.actions.on("remove", this.toggleDeleteButton)
        this.actions.on("add", this.toggleDeleteButton)
    },
    render: function () {
        this.$el.html(this.template({
            actionCount: this.options.actionCount,
            selectedAction: this.model.get("moduleKey"),
            actionModules: this.actionModules
        }));

        this.$(".action-module-key").change();
        this.toggleDeleteButton();
        return this.$el;
    },

    updateModel: function () {
        this.model.setFormParams(this.$el.serializeObject());
    },

    actionChanged: function () {
        var actionModuleKey = this.$(".action-module-key").val();
        var $throbber = this.$(".throbber");
        var $remoteParams = this.$(".remote-params");

        $throbber.addClass("loading");
        $remoteParams.addClass("loading-action");

        this.model.set("moduleKey", actionModuleKey);
        var url = AJS.contextPath() + "/rest/automation/1.0/config/";
        var ruleModel = this.options.configModel.get("rule");
        if (!ruleModel.isNew() && !this.model.isNew()) {
            url += ruleModel.getId() + "/action/" + this.model.getId() + "/";
        } else {
            url += "action/";
        }
        url += actionModuleKey;
        AJS.$.ajax({
            url: url,
            data: {actor: ruleModel.get("actor") },
            success: function (resp) {
                $throbber.removeClass("loading");
                $remoteParams.html(resp).removeClass("loading-action");

                //trigger an event in case actions depend on custom js.
                AJS.$(AJS).trigger("AJS.automation.form.loaded", $remoteParams);
            }
        });
    },

    removeAction: function (e) {
        e.preventDefault();
        if (confirm(AJS.I18n.getText('automation.plugin.action.remove.confirm'))) {
            this.actions.remove(this.model);
            this.model.off("saveFormState", this.updateModel);
            this.actions.off("remove", this.toggleDeleteButton)
            this.actions.off("add", this.toggleDeleteButton)
            this.remove();
        }
    },

    toggleDeleteButton: function () {
        if (this.actions.length === 1) {
            this.$(".delete-action-button").hide();
        } else {
            this.$(".delete-action-button").show();
        }
    }
});