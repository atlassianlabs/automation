AJS.namespace("Atlassian.Automation.RuleListView");
AJS.namespace("Atlassian.Automation.GlobalRuleView");
AJS.namespace("Atlassian.Automation.RuleRowView");
AJS.namespace("Atlassian.Automation.RuleDetailView");
AJS.namespace("Atlassian.Automation.SelectionModel");

AJS.namespace("Atlassian.Automation.RuleRouter");

Atlassian.Automation.SelectionModel = Brace.Model.extend({
    namedAttributes: ["selection"]
});

Atlassian.Automation.RuleRouter = Backbone.Router.extend({
   routes: {
       "rule/:id" : "getRule"
   }
});

Atlassian.Automation.GlobalRuleView = Brace.View.extend({
    template: Atlassian.Templates.Automation.ruleListContainer,

    events: {
    },

    initialize: function () {
        _.bindAll(this, 'renderNoRules', 'render');

        this.selectionModel = this.options.selectionModel;
        this.model.on("reset", this.render);
        var that = this;
        this.model.on("destroy", function() {
            that.selectionModel.setSelection(that.model.first());
        });

        this.selectionModel.on("change:selection", function() {
            //destroy the old detail view first.
            if(that.ruleDetailView) {
                that.ruleDetailView.undelegateEvents();
            }

            //make sure we render an empty message if there's nothing to render.
            if(that.selectionModel.getSelection() === undefined) {
                that.renderNoRules();
            } else {
                that.ruleDetailView = new Atlassian.Automation.RuleDetailView({
                    el: that.$(".automation-details"),
                    model: that.selectionModel.getSelection(),
                    renderOps:true
                });
                that.ruleDetailView.render();
            }
        });
    },

    renderNoRules: function() {
        this.$el.html(Atlassian.Templates.Automation.noRulesYet());
        return this;
    },

    render: function () {

        if(this.model.length === 0) {
            //no rules configured yet!
            this.renderNoRules();
        } else {

            this.$el.html(this.template());

            this.ruleList = new Atlassian.Automation.RuleListView({
                el: this.$(".automation-list"),
                model: this.model,
                selectionModel: this.selectionModel
            });

            this.ruleList.render();

            //make sure we have something selected before rendering!
            if(this.selectionModel.getSelection() === undefined) {
                this.selectionModel.setSelection(this.model.first());
            }
        }

        this.$el.addClass("rules-loaded");
        return this;
    }
});

Atlassian.Automation.RuleListView = Brace.View.extend({
    template: Atlassian.Templates.Automation.ruleList,

    events: {
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'render');

        //the model is a Atlassian.Automation.RuleCollection
        this.selectionModel = this.options.selectionModel;
        this.model.on("add", this.addOne);
    },

    addOne: function (ruleModel) {
        var rowView = new Atlassian.Automation.RuleRowView({
            model: ruleModel,
            selectionModel: this.selectionModel
        });
        this.$("tbody").append(rowView.render().$el);
    },

    render: function () {
        this.$el.html(this.template());
        var that = this;
        this.model.each(function(model) {
            that.addOne(model);
        });
        return this;
    }
});

Atlassian.Automation.RuleRowView = Brace.View.extend({
    tagName: "tr",
    template: Atlassian.Templates.Automation.ruleRow,

    events: {
        "click":"selectRule"
    },

    initialize: function () {
        _.bindAll(this, 'render', 'selectRule', 'updateSelection', 'remove');

        this.selectionModel = this.options.selectionModel;
        this.selectionModel.on("change:selection", this.updateSelection);
        this.model.on("change", this.render);
        this.model.on("destroy", this.remove);
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.updateSelection();

        this.$el.attr("data-id", this.model.get("id"));
        this.$el.removeClass("rule-enabled").removeClass("rule-disabled");
        if(this.model.get("enabled")) {
            this.$el.addClass("rule-enabled");
        } else {
            this.$el.addClass("rule-disabled");
        }

        return this;
    },

    selectRule:function() {
        this.selectionModel.setSelection(this.model);
    },

    updateSelection: function () {
        if (this.selectionModel.getSelection() === this.model) {
            this.$el.addClass("highlighted");
        } else {
            this.$el.removeClass("highlighted");
        }
    }
});

Atlassian.Automation.RuleDetailView = Brace.View.extend({
    template: Atlassian.Templates.Automation.ruleDetail,

    events: {
        "click #automation-delete": "deleteRule",
        "click .rule-status-toggle": "toggleRuleStatus"
    },

    initialize: function () {
        _.bindAll(this, 'render', 'deleteRule', 'toggleRuleStatus');

        this.renderOps = this.options.renderOps;

        this.model.on("change:enabled", this.render);
    },

    deleteRule: function(e) {
        e.preventDefault();
        var that = this;
        if(confirm(AJS.I18n.getText("automation.plugin.delete.confirm"))) {
            this.model.destroy({
                success:function() {
                    that.undelegateEvents();
                },
                error:function() {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                }
            });
        }
    },

    toggleRuleStatus: function(e) {
        e.preventDefault();

        var confirmMessage = AJS.I18n.getText("automation.plugin.disable.confirm");
        var enabled = this.model.get("enabled");
        var that = this;
        if(!enabled) {
            confirmMessage = AJS.I18n.getText("automation.plugin.enable.confirm");
        }

        var $throbber = this.$(".buttons-container .throbber");
        if (confirm(confirmMessage)) {
            $throbber.addClass("loading");
            AJS.$.ajax({
                url: this.$(".rule-status-toggle").attr("href"),
                type: "PUT",
                contentType: "application/json",
                success: function (resp) {
                    that.model.set("enabled", !enabled);
                },
                error: function (resp) {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                },
                complete:function() {
                    $throbber.removeClass("loading");
                }
            });
        }
    },

    render: function () {
        this.$el.html(this.template({
            rule: this.model.toJSON(),
            renderOps:this.renderOps
        }));

        var offset = this.$el.offset().top;

        var visible_area_start = AJS.$(window).scrollTop();
        var visible_area_end = visible_area_start + window.innerHeight;

        if (offset < visible_area_start || offset > visible_area_end) {
            // Not in view so scroll to it
            var $ruleDetailForm = this.$el.find("#rule-detail-form");

            // make sure we do not overflow the form's bottom
            var formBottom = $ruleDetailForm.height() + visible_area_start;
            var containerBottom = this.$el.offset().top + this.$el.height();
            var adjustedTop = visible_area_start - Math.max(formBottom - containerBottom, 0);

            // margin is relative to the container - subtract the top of the form to compensate
            adjustedTop = adjustedTop - $ruleDetailForm.offset().top;
            $ruleDetailForm.css({"margin-top": adjustedTop});
        }

        return this;
    }
});