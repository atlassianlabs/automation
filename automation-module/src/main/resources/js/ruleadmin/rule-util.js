//sigh...would be nice if this was in AUI!!!
if (typeof AJS.namespace === 'undefined') {
    AJS.namespace = function (namespace, context, value) {
        var names = namespace.split(".");
        context = context || window;
        for (var i = 0, n = names.length - 1; i < n; i++) {
            var x = context[names[i]];
            context = (x != null) ? x : context[names[i]] = {};
        }
        return context[names[i]] = value || {};
    };
}