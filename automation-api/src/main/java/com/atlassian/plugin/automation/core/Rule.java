package com.atlassian.plugin.automation.core;

import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;

/**
 * Represents a fully configured rule consisting of a single trigger and multiple actions to apply to the items returned
 * by the trigger.
 */
public interface Rule
{
    /**
     * @return human-readable name of this action
     */
    String getName();

    /**
     * @return unique identifier for this action
     */
    Integer getId();

    /**
     * @return the trigger that applies to this rule. May not be null.
     */
    TriggerConfiguration getTriggerConfiguration();

    /**
     * @return the actions that apply to this rule. May not be null or empty.
     */
    Iterable<ActionConfiguration> getActionsConfiguration();

    /**
     * @return true if this rule is currently enabled.
     */
    boolean isEnabled();

    /**
     * @return the actor for this rule (his user key/name, depending on the product)
     */
    String getActor();
}
