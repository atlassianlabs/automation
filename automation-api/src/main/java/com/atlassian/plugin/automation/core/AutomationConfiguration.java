package com.atlassian.plugin.automation.core;

import java.util.List;
import java.util.Map;

/**
 * Base interface for configuration
 */
public interface AutomationConfiguration
{
    /**
     * Unique identifier for this action configuration
     *
     * @return the id of this action configuration
     */
    Integer getId();

    /**
     * Used to look up the plugin module that provides this Action implementation.  {@link
     * com.atlassian.plugin.automation.core.Action} objects will be constructed using this information.
     *
     * @return the complete plugin module key defining this Action
     */
    String getModuleKey();

    /**
     * Returns additional parameters to configure an action.  This could be a transition id and comment body for an
     * action that transitions an issue in JIRA with a comment.
     *
     * @return a map with optional parameters to define an automation action
     */
    Map<String, List<String>> getParameters();
}
