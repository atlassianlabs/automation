package com.atlassian.plugin.automation.core.auditlog;

/**
 * Currently this simply returns a string but in future may support i18n by provided an optional i18n key and
 * parameters needed to produce an i18nized audit log message.
 */
public interface AuditString
{
    String getString();
}
