package com.atlassian.plugin.automation.spi.scheduler;

/**
 * This service is used to change the currently logged user in the product's authentication context
 */
public interface UserAuthenticationContextService
{
    void setCurrentUser(String username);
    String getCurrentUser();
}
