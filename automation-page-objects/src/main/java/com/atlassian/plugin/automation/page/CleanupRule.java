package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.TestedProduct;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Remove all automation rules when we're done!
 */
public class CleanupRule extends TestWatcher
{
    private final TestedProduct product;

    public CleanupRule(TestedProduct product)
    {
        this.product = product;
    }

    @Override
    protected void finished(final Description description)
    {
        AdminPage adminPage = product.getPageBinder().navigateToAndBind(AdminPage.class);
        adminPage.deleteAllRules();
    }
}
