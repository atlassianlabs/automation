package com.atlassian.plugin.automation.page.util;

import org.openqa.selenium.WebDriver;

public class DialogUtils
{
    private static final long ALERT_WAIT_TIME = 500;

    public static void acceptDialog(WebDriver driver)
    {
        try
        {
            Thread.sleep(ALERT_WAIT_TIME);
            driver.switchTo().alert().accept();
            Thread.sleep(ALERT_WAIT_TIME);
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }
}
