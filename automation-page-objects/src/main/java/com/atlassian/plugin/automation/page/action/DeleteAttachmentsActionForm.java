package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:delete-attachments-action")
public class DeleteAttachmentsActionForm extends ActionForm
{
    @javax.inject.Inject
    private PageElementFinder elementFinder;

    public DeleteAttachmentsActionForm(PageElement container, String moduleKey)
    {
        super(container, moduleKey);
    }

    public DeleteAttachmentsActionForm securityLevel(final String securityLevel)
    {
        final PageElement element = container.find(By.className("drop"));
        element.click();

        final PageElement dropDown = elementFinder.find(By.xpath("//div[@class='ajs-layer select-menu box-shadow active']"));
        final PageElement selectedOption = dropDown.find(By.linkText(securityLevel));
        selectedOption.click();

        return this;
    }

    public DeleteAttachmentsActionForm ignoredAttachmentsPattern(final String pattern)
    {
        final PageElement element = container.find(By.id("jiraIgnoreAttachments"));
        element.type(pattern);
        return this;
    }
}
