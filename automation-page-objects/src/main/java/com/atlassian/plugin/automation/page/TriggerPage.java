package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.util.RuleFormUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class TriggerPage extends AbstractConfigForm
{
    @ElementBy (className = "remote-params")
    private PageElement remoteParams;

    @Inject
    private FormFactory formFactory;

    @WaitUntil
    public void isReady()
    {
        waitUntilTrue(remoteParams.timed().isPresent());
        waitUntilFalse(remoteParams.timed().hasClass("loading-trigger"));
    }

    public <T> T selectTrigger(Class<T> triggerForm)
    {
        return formFactory.build(triggerForm, elementFinder.find(By.id("form-container")));
    }

    public <T> T getTrigger(Class<T> triggerForm)
    {
        return formFactory.build(triggerForm, elementFinder.find(By.id("form-container")));
    }

    public ActionsForm next()
    {
        RuleFormUtils.waitForFormRerender();
        nextButton.click();
        return pageBinder.bind(ActionsForm.class);
    }
}
