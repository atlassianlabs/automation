package com.atlassian.plugin.automation.page;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class AdminPage extends AbstractJiraPage
{
    public static final String URI = "/plugins/servlet/automationrules";

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy (id = "rule-container")
    private PageElement ruleContainer;

    @ElementBy (className = "automation-list")
    private PageElement ruleList;

    @ElementBy (id = "add-rule")
    private PageElement addRuleButton;

    @ElementBy (id = "event-log-button")
    private PageElement eventLogButton;

    @ElementBy(id = "admin-log-button")
    private PageElement adminLogButton;

    @ElementBy (id = "automation-config-button")
    private PageElement configurationButton;

    @Override
    public TimedCondition isAt()
    {
        return ruleContainer.withTimeout(TimeoutType.SLOW_PAGE_LOAD).timed().hasClass("rules-loaded");
    }

    public int getRulesCount()
    {
        if (!ruleList.isPresent())
        {
            return 0;
        }
        return ruleList.findAll(By.cssSelector("tr.rule-enabled")).size() + ruleList.findAll(By.cssSelector("tr.rule-disabled")).size();
    }

    public int getEnabledRulesCount()
    {
        if (!ruleList.isPresent())
        {
            return 0;
        }
        return ruleList.findAll(By.cssSelector("tr.rule-enabled")).size();
    }

    public List<Rule> getRules()
    {
        if (!ruleList.isPresent())
        {
            return Collections.emptyList();
        }
        List<PageElement> rules = ruleList.findAll(By.cssSelector("tr.rule-enabled"));
        rules.addAll(ruleList.findAll(By.cssSelector("tr.rule-disabled")));
        return newArrayList(Iterables.transform(rules, new Function<PageElement, Rule>()
        {
            @Override
            public Rule apply(@Nullable final PageElement input)
            {
                return new Rule(input, pageBinder, elementFinder, driver);
            }
        }));
    }

    public ConfigRuleForm addRuleForm()
    {
        addRuleButton.click();
        return pageBinder.bind(ConfigRuleForm.class);
    }

    public EventAuditLogPage eventAuditLogPage()
    {
        eventLogButton.click();
        return pageBinder.bind(EventAuditLogPage.class);
    }

    public AdminAuditLogPage adminAuditLogPage()
    {
        adminLogButton.click();
        return pageBinder.bind(AdminAuditLogPage.class);
    }

    @Override
    public String getUrl()
    {
        return URI;
    }

    public ConfigurationPage configurationPage()
    {
        configurationButton.click();
        return pageBinder.bind(ConfigurationPage.class);
    }

    public void deleteAllRules()
    {
        for (Rule rule : getRules())
        {
            rule.delete();
        }
    }
}

