package com.atlassian.plugin.automation.page;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class ConfigurationPage extends AbstractJiraPage
{
    public static final String URI = "/plugins/servlet/automationconfig";

    @ElementBy(id = "config-form")
    private PageElement configForm;

    @ElementBy(id = "config-form-submit")
    private PageElement saveButton;

    @Override
    public TimedCondition isAt()
    {
        return configForm.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }

    public ConfigurationPage setRateLimiterEnabled(final boolean isEnabled)
    {
        final CheckboxElement checkbox = elementFinder.find(By.name("rate-limiter-enabled"), CheckboxElement.class);
        if (isEnabled)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }

    public ConfigurationPage setRuleLog(final boolean isEnabled)
    {
        final CheckboxElement checkbox = elementFinder.find(By.name("rule-log-enabled"), CheckboxElement.class);
        if (isEnabled)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }

    public boolean isRuleLogOptionVisible()
    {
        final PageElement ruleLog = elementFinder.find(By.name("rule-log-enabled"));
        return ruleLog.isPresent() && ruleLog.isVisible();
    }

    public ConfigurationPage save()
    {
        saveButton.click();
        final PageElement throbber = configForm.find(By.className("throbber"));
        waitUntilFalse(throbber.timed().hasClass("loading"));
        return this;
    }
}
