package com.atlassian.plugin.automation.page;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

public class AdminAuditLogPage extends AbstractJiraPage
{
    public static final String URI = "/plugins/servlet/automationadminauditlog";

    @ElementBy(id = "audit-message-list")
    private PageElement auditLogList;

    public int getLogCount()
    {
        return auditLogList.findAll(By.cssSelector("tr.audit-message")).size();
    }

    @Override
    public TimedCondition isAt()
    {
        return auditLogList.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }
}
