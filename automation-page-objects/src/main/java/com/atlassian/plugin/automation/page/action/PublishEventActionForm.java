package com.atlassian.plugin.automation.page.action;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;


@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:publish-event-action")
public class PublishEventActionForm extends ActionForm
{
    public PublishEventActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }


    public PublishEventActionForm setEvent(String eventName)
    {
        final PageElement parentElement = container.find(By.className("jira-publish-event-action"));
        waitUntilTrue(parentElement.timed().isPresent());
        final SingleSelect eventSelect = pageBinder.bind(SingleSelect.class, parentElement);
        eventSelect.select(eventName);
        return this;
    }
}
