package com.atlassian.plugin.automation.config;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.auditlog.AdminAuditLogService;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.CronTrigger;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.scheduler.AutomationScheduler;
import com.atlassian.plugin.automation.service.RuleErrors;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.google.common.collect.Lists;
import net.java.ao.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.config.ao.CurrentSchema.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith (MockitoJUnitRunner.class)
public class TestDefaultRuleService
{
    @Mock
    private ActiveObjects ao;
    @Mock
    private UserManager userManager;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private AutomationScheduler automationScheduler;
    @Mock
    private AutomationModuleManager moduleManager;
    @Mock
    private CronTrigger trigger;
    @Mock
    private Action action;
    @Mock
    private AdminAuditLogService adminAuditLogService;

    private DefaultRuleService service;

    @Before
    public void setup()
    {
        service = new DefaultRuleService(i18nResolver, userManager, adminAuditLogService, automationScheduler, new AORuleConfigStore(ao), moduleManager);
        when(moduleManager.getAction(anyString())).thenReturn(action);
        when(moduleManager.getTrigger(anyString())).thenReturn(trigger);

        when(trigger.validateAddConfiguration(any(I18nResolver.class), any(Map.class), anyString())).thenReturn(new ErrorCollection());
        when(action.validateAddConfiguration(any(I18nResolver.class), any(Map.class), anyString())).thenReturn(new ErrorCollection());

        // No rule with the same name exists
        when(ao.find((Class) any(), (Query) any())).thenReturn(new RuleEntity[0]);

        // Return valid user
        when(userManager.getUserProfile(anyString())).thenReturn(mock(UserProfile.class));
        when(userManager.isSystemAdmin(anyString())).thenReturn(true);

        when(i18nResolver.getText(anyString())).thenReturn("error");
    }

    @Test
    public void testValidateFailsName()
    {
        Rule rule = getDefaultRule();

        when(rule.getName()).thenReturn(null);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);
        assertTrue("Rule name check failed", result.isLeft());
        assertTrue("Name validation error", getError(getRuleErrors(result), "name"));
    }

    @Test
    public void testValidateFailsPermission()
    {
        Rule rule = getDefaultRule();

        when(userManager.isSystemAdmin(anyString())).thenReturn(false);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);
        assertTrue("Permission check didn't fail", result.isLeft());
        assertFalse("Permission validation error", getRuleErrors(result).getErrorMessages().isEmpty());
    }

    @Test
    public void testValidateFailsTriggerValidation()
    {
        Rule rule = getDefaultRule();

        final ErrorCollection errorCollectionWithErrors = new ErrorCollection();
        errorCollectionWithErrors.addError("trigger", "value invalid");
        when(trigger.validateAddConfiguration(any(I18nResolver.class), any(Map.class), anyString())).thenReturn(errorCollectionWithErrors);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);

        assertTrue("Invalid trigger data not rejected", result.isLeft());
        assertTrue("Params validation error", getError(getTriggerErrors(result), "trigger"));
    }

    @Test
    public void testValidateFailsTriggerNotFound()
    {
        Rule rule = getDefaultRule();

        final ErrorCollection errorCollectionWithErrors = new ErrorCollection();
        errorCollectionWithErrors.addError("trigger", "value invalid");
        when(moduleManager.getTrigger(anyString())).thenReturn(null);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);

        assertTrue("Invalid trigger not rejected", result.isLeft());
        assertTrue("Trigger module validation error", getError(getTriggerErrors(result), "moduleKey"));
    }

    @Test
    public void testValidateFailsActionValidation()
    {
        Rule rule = getDefaultRule();

        final ErrorCollection errorCollectionWithErrors = new ErrorCollection();
        errorCollectionWithErrors.addError("action", "value invalid");
        when(action.validateAddConfiguration(any(I18nResolver.class), any(Map.class), anyString())).thenReturn(errorCollectionWithErrors);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);

        assertTrue("Invalid action data not rejected", result.isLeft());
        assertTrue("Action validation error", getError(getFirstActionErrors(result), "action"));
    }

    @Test
    public void testValidateFailsActionNotFound()
    {
        Rule rule = getDefaultRule();

        final ErrorCollection errorCollectionWithErrors = new ErrorCollection();
        errorCollectionWithErrors.addError("trigger", "value invalid");
        when(moduleManager.getAction(anyString())).thenReturn(null);
        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);

        assertTrue("Invalid action not rejected", result.isLeft());
        assertTrue("Action module validation error", getError(getFirstActionErrors(result), "moduleKey"));
    }

    @Test
    public void testCannotAddDuplicateRule()
    {
        Rule rule = getDefaultRule();

        when(rule.getId()).thenReturn(19);
        final RuleEntity mockRuleEntity = getMockRuleEntity();

        // One rule with that name should already exist
        when(ao.find((Class) any(), (Query) any())).thenReturn(new RuleEntity[] { mockRuleEntity });

        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);
        assertTrue("Rule name check failed", result.isLeft());
        assertTrue("Name validation error", getError(getRuleErrors(result), "name"));
    }

    @Test
    public void testUpdateDuplicateDetection()
    {
        Rule rule = getDefaultRule();

        final RuleEntity mockRuleEntity = getMockRuleEntity();

        // One rule with that name should already exist
        when(ao.find((Class) any(), (Query) any())).thenReturn(new RuleEntity[] { mockRuleEntity });

        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);
        assertTrue("No validation errors", result.isRight());
    }

    @Test
    public void testInvalidActor()
    {
        Rule rule = getDefaultRule();

        // Return invalid user
        when(userManager.getUserProfile(anyString())).thenReturn(null);

        Either<RuleErrors, RuleService.RuleValidationResult> result = service.validateAddRule("user", rule);
        assertTrue("Rule actor check failed", result.isLeft());
        assertTrue("Name validation error", getError(getRuleErrors(result), "actor"));
    }

    @Test
    public void testPartialValidation()
    {
        Rule rule = getDefaultRule();

        // Actions return error, but this should work fine cause we only validate triggers
        final ErrorCollection errorCollectionWithErrors = new ErrorCollection();
        errorCollectionWithErrors.addError("action", "value invalid");
        when(action.validateAddConfiguration(any(I18nResolver.class), any(Map.class), anyString())).thenReturn(errorCollectionWithErrors);

        // Return invalid user, but this should work fine cause we only validate triggers
        when(userManager.getUserProfile(anyString())).thenReturn(null);

        RuleErrors result = service.validatePartialRule("user", rule, 1);
        assertFalse("Step 1 check failed", result.hasAnyErrors());
    }

    @Test
    public void testPartialValidationInvalidStep()
    {
        Rule rule = getDefaultRule();
        RuleErrors result = service.validatePartialRule("user", rule, -1);
        assertTrue("Step -1 check failed", result.hasAnyErrors());

        result = service.validatePartialRule("user", rule, 3);
        assertTrue("Step -1 check failed", result.hasAnyErrors());
    }

    private RuleEntity getMockRuleEntity()
    {
        final RuleEntity mockRuleEntity = mock(RuleEntity.class);
        final TriggerEntity mockTriggerEntity = mock(TriggerEntity.class);
        when(mockTriggerEntity.getTriggerConfiguration()).thenReturn(new TriggerConfigEntity[0]);
        when(mockRuleEntity.getActions()).thenReturn(new ActionEntity[0]);
        when(mockRuleEntity.getTrigger()).thenReturn(mockTriggerEntity);
        return mockRuleEntity;
    }

    private Rule getDefaultRule()
    {
        Rule rule = mock(Rule.class);
        when(rule.getName()).thenReturn("Name");
        when(rule.getActor()).thenReturn("admin");
        when(rule.isEnabled()).thenReturn(true);

        TriggerConfiguration triggerConfiguration = mock(TriggerConfiguration.class);
        when(triggerConfiguration.getModuleKey()).thenReturn("com.atlassian.some.module.key");

        when(rule.getTriggerConfiguration()).thenReturn(triggerConfiguration);
        ActionConfiguration actionConfiguration = mock(ActionConfiguration.class);
        List<ActionConfiguration> actionConfigurations = Lists.newArrayList();
        actionConfigurations.add(actionConfiguration);

        when(rule.getActionsConfiguration()).thenReturn(actionConfigurations);

        return rule;
    }

    private boolean getError(final ErrorCollection errors, String errorKey)
    {
        return errors.getErrors().containsKey(errorKey);
    }

    private ErrorCollection getFirstActionErrors(final Either<RuleErrors, RuleService.RuleValidationResult> result)
    {
        return result.left().get().getActionErrors().iterator().next();
    }

    private ErrorCollection getRuleErrors(final Either<RuleErrors, RuleService.RuleValidationResult> result)
    {
        return result.left().get().getRuleErrors();
    }

    private ErrorCollection getTriggerErrors(final Either<RuleErrors, RuleService.RuleValidationResult> result)
    {
        return result.left().get().getTriggerErrors();
    }
}
