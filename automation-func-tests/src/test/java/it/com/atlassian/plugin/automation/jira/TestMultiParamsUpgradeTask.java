package it.com.atlassian.plugin.automation.jira;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.Rule;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestMultiParamsUpgradeTask extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        //this data contains some rules that were created using the AO database schema before
        //it supported multivalue params.
        backdoor().dataImport().restoreDataFromResource("multiparams-upgrade-test.zip");
    }

    @Test
    public void testRulesUpgraded()
    {
        final AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 2, adminPage.getRulesCount());
        final List<Rule> rules = adminPage.getRules();

        final Rule cats = rules.get(0);
        final Rule cows = rules.get(1);

        assertEquals("Cats", cats.getName());
        assertTrue(cats.isEnabled());
        assertEquals("Issue Event Trigger", cats.getTrigger().getType());
        final Map<String, String> triggerParams = cats.getTrigger().getParams();
        assertEquals("ISSUE RESOLVED", triggerParams.get("jiraEventId"));
        assertEquals("text ~ \"stuff\"", triggerParams.get("jiraJqlExpression"));

        final List<Rule.Action> catActions = cats.getActions();
        assertEquals(1, catActions.size());
        final Rule.Action commentIssueAction = catActions.get(0);
        assertAction(commentIssueAction, "COMMENT ISSUE ACTION",
                EasyMap.build("jiraCommentNotification", "Yes", "jiraComment", "Hello World!", "jiraCommentSecurityLevel", "Restricted to Users"));

        assertEquals("Cows", cows.getName());
        assertTrue(cows.isEnabled());
        assertEquals("JQL Filter Trigger", cows.getTrigger().getType());
        final Map<String, String> jqltriggerParams = cows.getTrigger().getParams();
        assertEquals("project = test", jqltriggerParams.get("jiraJqlExpression"));
        assertEquals("1000", jqltriggerParams.get("jiraMaxResults"));
        assertEquals("*/5 * * * * ?", jqltriggerParams.get("cronExpression"));

        final List<Rule.Action> cowActions = cows.getActions();
        assertEquals(4, cowActions.size());
        assertAction(cowActions.get(0), "SET ASSIGNEE TO LAST COMMENTED ACTION", EasyMap.build("groupMembership", "Unrestricted", "excludedUsers", "None"));
        assertAction(cowActions.get(1), "COMMENT ISSUE ACTION", EasyMap.build("jiraCommentNotification", "Yes", "jiraComment", "Test", "jiraCommentSecurityLevel", "Restricted to Users"));
        assertAction(cowActions.get(2), "TRANSITION ISSUE ACTION", EasyMap.build("jiraActionId", "jira: Create Issue (1)", "jiraTransitionFields", "description=automated transition", "disableTransitionNotification", "No", "skipConditions", "No"));
        assertAction(cowActions.get(3), "EDIT ISSUE ACTION", EasyMap.build("jiraEditFields", "description=automated edit", "jiraEditNotification", "No", "jiraAllowVariableExpansion", "No"));
    }

    private void assertAction(Rule.Action action, String type, Map params)
    {
        assertThat("Invalid Action type for action", type, equalToIgnoringCase(action.getType()));
        assertEquals("Invalid Action parameters", params, action.getParams());
    }
}
