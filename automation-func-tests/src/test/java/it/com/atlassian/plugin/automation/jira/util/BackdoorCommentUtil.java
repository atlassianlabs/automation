package it.com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;

public class BackdoorCommentUtil
{
    public static void addComment(JiraTestedProduct jira, String issueKey, String commentText)
    {
        // We need to post comment like this due to bug in backdoor()
        final CommentClient commentClient = new CommentClient(jira.environmentData());
        final Comment comment = new Comment();
        comment.body = commentText;
        commentClient.post(issueKey, comment);
    }
}
