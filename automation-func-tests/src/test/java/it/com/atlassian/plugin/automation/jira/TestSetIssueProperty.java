package it.com.atlassian.plugin.automation.jira;

import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.action.SetIssuePropertyForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import com.atlassian.plugin.automation.page.trigger.IssuePropertySetEventTriggerForm;
import com.sun.jersey.api.client.WebResource;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import it.com.atlassian.plugin.automation.jira.util.IntegrationTestVersionUtil;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestSetIssueProperty extends AbstractAutomationTest {
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup() {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        jira().quickLoginAsAdmin();
    }

    @Test
    public void testSetIssueProperty() throws Exception {
        final int buildNumber = IntegrationTestVersionUtil.getJiraBuildNumber(jira().environmentData().getBaseUrl().toString());

        if (buildNumber >= IntegrationTestVersionUtil.JIRA_62_BUILD_NUMBER) {
            AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

            TriggerPage triggerPage = adminPage.addRuleForm().
                    ruleName("set property").
                    enabled(true).
                    next();

            triggerPage.selectTrigger(IssueEventTriggerForm.class).
                    setEvents("Issue Created");
            ActionsForm actionsForm = triggerPage.next();
            final String propertyKey = "foo.blah";
            final int propertyValue = 1;
            actionsForm.setInitialAction(SetIssuePropertyForm.class).propertyKey(propertyKey).propertyValue(String.valueOf(propertyValue));
            adminPage = actionsForm.next().save();

            triggerPage = adminPage.addRuleForm().
                    ruleName("issue event property").
                    enabled(true).
                    next();

            triggerPage.selectTrigger(IssuePropertySetEventTriggerForm.class).
                    setEventPropertyKeys(propertyKey);
            actionsForm = triggerPage.next();
            actionsForm.setInitialAction(CommentIssueActionForm.class).comment("Added comment");
            adminPage = actionsForm.next().save();

            assertEquals("No rule was added", 2, adminPage.getRulesCount());

            final String issueKey = jira().backdoor().issues().createIssue("TEST", "Hello").key();

            // comment should be added due to the publish event action
            waitUntilTrue("comment should be added", new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

            final JSONObject issueProperty = getIssueProperty(issueKey, propertyKey);
            assertEquals("property not set correctly", propertyValue, issueProperty.getInt("value"));
        }
    }

    private JSONObject getIssueProperty(String issueKey, String propertyKey) throws Exception {
        final String url = String.format("issue/%s/properties/%s", issueKey, propertyKey);
        final WebResource.Builder builder = backdoor().rawRestApiControl().rootResource().path(url).getRequestBuilder();
        builder.accept(MediaType.APPLICATION_JSON_TYPE);
        builder.type(MediaType.APPLICATION_JSON_TYPE);
        return new JSONObject(builder.get(String.class));
    }
}
