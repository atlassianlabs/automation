package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.EditUserProfileActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import it.com.atlassian.plugin.automation.jira.util.BackdoorCommentUtil;
import it.com.atlassian.plugin.automation.jira.util.CustomViewProfilePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEditUserProfile extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());


    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
    }

    @After
    public void tearDown()
    {
        jira().quickLoginAsSysadmin();
    }

    @Test
    public void testEditUserPropertiesActionWithOverwrite()
    {
        AdminPage automationAdmin = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        final TriggerPage triggerPage = automationAdmin.addRuleForm().enabled(true).ruleName("test").ruleActor("admin").next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Commented");
        ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditUserProfileActionForm.class).propertyKey("jira.user.timezone").propertyValue("Asia/Tokyo").shouldOverwrite(true);
        actionsForm.next().save();

        CustomViewProfilePage profilePage = jira().goTo(CustomViewProfilePage.class);
        assertFalse("Profile shouldn't contain Tokyo", profilePage.getTimezone().contains("Tokyo"));

        jira().quickLoginAsSysadmin();

        final IssueCreateResponse issueResponse = jira().backdoor().issues().createIssue("TEST", "Hello World");
        final String issueKey = issueResponse.key;
        BackdoorCommentUtil.addComment(jira(), issueKey, "test");
        Poller.waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

        profilePage = jira().goTo(CustomViewProfilePage.class);
        assertTrue("Profile should contain Tokyo", profilePage.getTimezone().contains("Tokyo"));

        // reconfigure the rule to set different value, so it should overwrite the value set before ("Tokyo")
        automationAdmin = jira().goTo(AdminPage.class);
        actionsForm = automationAdmin.getRules().get(0).edit().next().next();
        actionsForm.getInitialAction(EditUserProfileActionForm.class).propertyValue("Europe/Berlin");
        actionsForm.next().save();

        BackdoorCommentUtil.addComment(jira(), issueKey, "test again (should set the user profile)");
        Poller.waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

        profilePage = jira().goTo(CustomViewProfilePage.class);
        assertTrue("Profile should contain Berlin", profilePage.getTimezone().contains("Berlin"));
    }

    @Test
    public void testEditUserPropertiesActionWithoutOverwrite()
    {
        AdminPage automationAdmin = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        final TriggerPage triggerPage = automationAdmin.addRuleForm().enabled(true).ruleName("test").ruleActor("admin").next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Commented");
        ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditUserProfileActionForm.class).propertyKey("jira.user.timezone").propertyValue("Asia/Tokyo").shouldOverwrite(false);
        actionsForm.next().save();

        jira().backdoor().usersAndGroups().addUser("user");
        jira().quickLogin("user", "user");

        IssuesControl userIssues = new IssuesControl(jira().environmentData(), new IssueTypeControl(jira().environmentData())).loginAs("user");
        final IssueCreateResponse issueResponse = userIssues.createIssue("TEST", "test user issue");
        String issueKey = issueResponse.key;

        CustomViewProfilePage profilePage = jira().goTo(CustomViewProfilePage.class);
        assertFalse("Profile shouldn't contain Tokyo", profilePage.getTimezone().contains("Tokyo"));

        jira().quickLoginAsSysadmin();
        BackdoorCommentUtil.addComment(jira(), issueKey, "test");
        Poller.waitUntilTrue(new IssueCommentedCondition(userIssues, issueKey, 1));

        // reconfigure the rule to set different value, so it should overwrite the value set before ("Tokyo")
        automationAdmin = jira().goTo(AdminPage.class);
        actionsForm = automationAdmin.getRules().get(0).edit().next().next();
        actionsForm.getInitialAction(EditUserProfileActionForm.class).propertyValue("Europe/Berlin");
        actionsForm.next().save();

        jira().quickLogin("user", "user");
        profilePage = jira().goTo(CustomViewProfilePage.class);
        assertTrue("Profile should contain Tokyo", profilePage.getTimezone().contains("Tokyo"));
        BackdoorCommentUtil.addComment(jira(), issueKey, "test again (should not set the user profile)");

        jira().quickLoginAsSysadmin();
        Poller.waitUntilTrue(new IssueCommentedCondition(userIssues, issueKey, 2));

        jira().quickLogin("user", "user");
        profilePage = jira().goTo(CustomViewProfilePage.class);
        assertTrue("Profile should still contain Tokyo", profilePage.getTimezone().contains("Tokyo"));
    }
}
