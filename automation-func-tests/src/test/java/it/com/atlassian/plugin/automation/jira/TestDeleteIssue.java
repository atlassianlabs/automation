package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.DeleteIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueNotPresentCondition;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestDeleteIssue extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());
    private String issueKey;

    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        jira().backdoor().project().addProject("Another Test", "TESTTWO", "admin");

        final IssueCreateResponse issueResponse = jira().backdoor().issues().createIssue("TESTTWO", "Hello World");
        issueKey = issueResponse.key;
    }

    @Test
    public void testDeleteIssues()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Updated");

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(DeleteIssueActionForm.class)
                .dispatchEvent(false);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        //lets trigger the issue edited event.
        jira().backdoor().issues().setDescription(issueKey, "add a description to trigger edited event!");

        waitUntilTrue("issue should not be present", new IssueNotPresentCondition(jira().backdoor().search(), issueKey));
    }
}
