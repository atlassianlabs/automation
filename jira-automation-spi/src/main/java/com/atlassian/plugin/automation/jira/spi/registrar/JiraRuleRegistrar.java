package com.atlassian.plugin.automation.jira.spi.registrar;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.plugin.automation.jira.spi.scheduler.JiraCronScheduler;
import com.atlassian.plugin.automation.spi.registrar.RuleModificationListener;
import com.atlassian.plugin.automation.spi.registrar.RuleRegistrar;
import com.atlassian.plugin.automation.spi.scheduler.CronScheduler;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.scheduling.PluginJob;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Contains the rule registrar implementation for JIRA. It communicates between clusters if some rule has been changed.
 * TODO in case we miss some cluster events, the registrar might get out-of-sync with the DB. We can either re-sync 1x hour or use atlassian-cache for this.
 */
@Named
@ExportAsService
@SuppressWarnings("unused")
public class JiraRuleRegistrar implements RuleRegistrar, InitializingBean, DisposableBean, ClusterMessageConsumer
{
    private static final Logger log = Logger.getLogger(JiraRuleRegistrar.class);

    // Limited up to 20 characters
    private static final String CHANNEL_NAME = "JIRA_ATM_CHANNEL";

    private final EventPublisher eventPublisher;
    private final ClusterMessagingService clusterMessagingService;
    private final CronScheduler cronScheduler;
    private final ConcurrentMap<Integer, RuleHandle> ruleHandlers = new ConcurrentHashMap<Integer, RuleHandle>();
    private final CopyOnWriteArraySet<RuleModificationListener> ruleModificationListeners = new CopyOnWriteArraySet<RuleModificationListener>();

    @Inject
    public JiraRuleRegistrar(@ComponentImport EventPublisher eventPublisher,
                             final JiraCronScheduler cronScheduler,
                             @ComponentImport final ClusterMessagingService clusterMessagingservice) throws Exception
    {
        this.eventPublisher = eventPublisher;
        this.clusterMessagingService = clusterMessagingservice;
        this.cronScheduler = cronScheduler;
    }

    @Override
    public void registerEventTriggeredRule(final int ruleId, final Object eventHandler, boolean notify)
    {
        log.debug("Registering event handler for rule id: " + ruleId);

        final RuleHandle alreadyPresentRuleHandle = ruleHandlers.put(ruleId, new EventRuleHandle(eventHandler));
        if (alreadyPresentRuleHandle != null)
        {
            log.warn("There is already present a stale handler for the rule id: " + ruleId);
            alreadyPresentRuleHandle.unschedule();
        }
        if (notify)
        {
            clusterMessagingService.sendRemote(CHANNEL_NAME, String.valueOf(ruleId));
        }
    }

    @Override
    public void registerCronTriggeredRule(int ruleId, Map<String, Object> jobParams, Class<? extends PluginJob> pluginJobClass, boolean notify)
    {
        log.debug("Registering CRON trigger for rule id: " + ruleId);

        final RuleHandle alreadyPresentRuleHandle = ruleHandlers.put(ruleId, new CronRuleHandle(String.format("AutomationRule-%d", ruleId), jobParams, pluginJobClass));
        if (alreadyPresentRuleHandle != null)
        {
            log.warn("There is already present a stale handler for the rule id: " + ruleId);
            alreadyPresentRuleHandle.unschedule();
        }
        if (notify)
        {
            clusterMessagingService.sendRemote(CHANNEL_NAME, String.valueOf(ruleId));
        }
    }

    @Override
    public void unregisterRule(int ruleId, boolean notify)
    {
        final RuleHandle ruleHandle = this.ruleHandlers.remove(ruleId);
        if (ruleHandle != null)
        {
            log.debug("Unregistering handler for rule id: " + ruleId);
            ruleHandle.unschedule();
            if (notify)
            {
                clusterMessagingService.sendRemote(CHANNEL_NAME, String.valueOf(ruleId));
            }
        }
    }

    @Override
    public void unregisterAllRules()
    {
        log.info("Unregistering all rules");
        for (Map.Entry<Integer, RuleHandle> entry : ruleHandlers.entrySet())
        {
            log.debug("Unscheduling handler for rule id: " + entry.getKey());
            entry.getValue().unschedule();
            clusterMessagingService.sendRemote(CHANNEL_NAME, String.valueOf(entry.getKey()));
        }
        ruleHandlers.clear();
    }

    @Override
    public boolean isRuleRegistered(int ruleId)
    {
        return ruleHandlers.containsKey(ruleId);
    }

    @Override
    public void registerRuleChangedHandler(final RuleModificationListener handler)
    {
        checkNotNull(handler);
        ruleModificationListeners.add(handler);
    }

    @Override
    public void unregisterRuleChangedHandler(final RuleModificationListener handler)
    {
        checkNotNull(handler);
        ruleModificationListeners.remove(handler);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        clusterMessagingService.registerListener(CHANNEL_NAME, this);
    }

    @Override
    public void destroy() throws Exception
    {
        clusterMessagingService.unregisterListener(CHANNEL_NAME, this);
    }

    @Override
    public void receive(String channel, String message, String senderId)
    {
        if (CHANNEL_NAME.equals(channel))
        {
            log.debug(String.format("Recieved message '%s' from '%s'", message, senderId));

            if (StringUtils.isNumeric(message))
            {
                final int ruleId = Integer.parseInt(message);
                for (final RuleModificationListener ruleModificationListener : ruleModificationListeners)
                {
                    ruleModificationListener.onRuleModified(ruleId);
                }
            }
        }
    }


    // this is a handle for keeping track of currently registered rules
    private interface RuleHandle
    {
        void unschedule();
    }

    private class EventRuleHandle implements RuleHandle
    {
        private final Object eventHandler;

        public EventRuleHandle(final Object eventHandler)
        {
            this.eventHandler = eventHandler;
            eventPublisher.register(eventHandler);
        }

        @Override
        public void unschedule()
        {
            try
            {
                eventPublisher.unregister(eventHandler);
            }
            catch (RuntimeException e)
            {
                log.error("Unexpected error while un-scheduling rule.", e);
            }
        }
    }

    private class CronRuleHandle implements RuleHandle
    {
        private final String cronTrigger;

        private CronRuleHandle(final String jobKey, Map<String, Object> jobParams, Class<? extends PluginJob> pluginJobClass)
        {
            this.cronTrigger = jobKey;
            cronScheduler.scheduleJob(jobKey, jobParams, pluginJobClass);
        }

        @Override
        public void unschedule()
        {
            try
            {
                cronScheduler.unscheduleJob(cronTrigger);
            }
            catch (RuntimeException e)
            {
                log.error("Unexpected error while un-scheduling rule.", e);
            }
        }
    }
}
