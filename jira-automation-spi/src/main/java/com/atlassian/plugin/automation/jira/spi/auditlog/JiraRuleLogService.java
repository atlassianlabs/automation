package com.atlassian.plugin.automation.jira.spi.auditlog;

import com.atlassian.jira.bc.issue.properties.IssuePropertyService;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.entity.property.EntityPropertyService.PropertyInput;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.spi.auditlog.RuleLogService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.DisposableBean;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@ExportAsService
@SuppressWarnings("unused")
public class JiraRuleLogService implements RuleLogService, DisposableBean
{
    private static final Logger log = Logger.getLogger(JiraRuleLogService.class);
    private static final String PROPERTY_KEY = "automation";

    private final UserManager userManager;
    private final ServiceTracker issuePropertyServiceTracker;

    @Inject
    public JiraRuleLogService(@ComponentImport final UserManager userManager, BundleContext bundleContext)
    {
        this.userManager = userManager;
        issuePropertyServiceTracker = new ServiceTracker(bundleContext, "com.atlassian.jira.bc.issue.properties.IssuePropertyService", null);
        issuePropertyServiceTracker.open();
    }

    @Override
    public boolean isAvailable()
    {
        return issuePropertyServiceTracker.getService() != null;
    }

    @Override
    public void logRuleExecution(final Rule rule, Object associatedItem)
    {
        if (!(associatedItem instanceof Issue))
        {
            // nothing to do here
            return;
        }
        final ApplicationUser actorUser = userManager.getUserByName(rule.getActor());
        if (actorUser == null)
        {
            log.error(String.format("Unable to add logging as actor does not exist. Rule: %d, actor: %s", rule.getId(), rule.getActor()));
            return;
        }
        final Issue associatedIssue = (Issue) associatedItem;
        final IssuePropertyService issuePropertyService = (IssuePropertyService) issuePropertyServiceTracker.getService();
        final EntityPropertyService.PropertyResult getPropertyResult = issuePropertyService.getProperty(actorUser, associatedIssue.getId(), PROPERTY_KEY);
        if (!getPropertyResult.isValid())
        {
            log.error(String.format("Unable to get the property on issue '%s' (rule: %d)", associatedIssue.getKey(), rule.getId()));
            return;
        }
        final PropertyInput propertyInput = getPropertyResult.getEntityProperty().fold(
                new Supplier<PropertyInput>()
                {
                    @Override
                    public PropertyInput get()
                    {
                        return new PropertyInput(new JiraEntityLogEntry(DateTime.now(), getLogEntry(rule)).getJSONString(), PROPERTY_KEY);
                    }
                }, new Function<EntityProperty, PropertyInput>()
                {
                    @Override
                    public PropertyInput apply(EntityProperty input)
                    {
                        return new PropertyInput(new JiraEntityLogEntry(DateTime.now(), getLogEntry(rule), input.getValue()).getJSONString(), PROPERTY_KEY);
                    }
                });
        final EntityPropertyService.SetPropertyValidationResult setPropertyValidationResult = issuePropertyService.validateSetProperty(actorUser,
                associatedIssue.getId(), propertyInput);
        if (!setPropertyValidationResult.isValid())
        {
            log.error(String.format("Unable to validate the property on issue '%s' (rule: %d)", associatedIssue.getKey(), rule.getId()));
            if (log.isDebugEnabled())
            {
                log.debug(String.format("Errors: %s", StringUtils.join(setPropertyValidationResult.getErrorCollection().getErrorMessages(), ";")));
            }
            return;
        }
        final EntityPropertyService.PropertyResult propertyResult = issuePropertyService.setProperty(actorUser, setPropertyValidationResult);
        if (!propertyResult.isValid())
        {
            log.error(String.format("Unable to set the property on issue '%s' (rule: %d)", associatedIssue.getKey(), rule.getId()));
            if (log.isDebugEnabled())
            {
                log.debug(String.format("Errors: %s", StringUtils.join(propertyResult.getErrorCollection().getErrorMessages(), ";")));
            }
        }
    }

    @Override
    public void destroy() throws Exception
    {
        issuePropertyServiceTracker.close();
    }

    private static String getLogEntry(Rule rule)
    {
        return rule.getName();
    }
}

