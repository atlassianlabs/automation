(function () {
    AJS.namespace("AJS.automation.AjaxGroupPickerUtils");

    AJS.automation.AjaxGroupPickerUtils.singleSelect = function(fieldID, context) {
        groupSelect(fieldID, context, false);
    };

    AJS.automation.AjaxGroupPickerUtils.multiSelect = function(fieldID, context) {
        groupSelect(fieldID, context, true);
    };

    var groupSelect = function(fieldID, context, isMultiSelect) {
        var options = {
            element: AJS.$("#" + fieldID, context),
            itemAttrDisplayed: "label",
            showDropdownButton: false,
            errorMessage: "The group \'\'{0}\'\' is not valid, it will be ignored",
            ajaxOptions: {
                url: function () {
                    return AJS.contextPath() + "/rest/api/2/groups/picker"
                },
                query: true, // keep going back to the sever for each keystroke
                minQueryLength: 1,
                formatResponse: function (response) {
                    var ret = [];

                    AJS.$(response).each(function (i, suggestions) {
                        var groupDescriptor = new AJS.GroupDescriptor({
                            weight: i,
                            label: suggestions.footer
                        });
                        AJS.$(suggestions.groups).each(function () {
                            groupDescriptor.addItem(new AJS.ItemDescriptor({
                                value: this.name,
                                label: this.name,
                                html: this.html,
                                allowDuplicate: false,
                                highlighted: true
                            }))
                        });
                        ret.push(groupDescriptor)
                    });

                    return ret;
                }
            }
        }

        if(isMultiSelect) {
            new AJS.MultiSelect(options);
        } else {
            new AJS.SingleSelect(options);
        }

    };
})();
