(function () {
    var init = function(e, $context) {
        new AJS.SecurityLevelSelect(AJS.$(".comment-action-area .commentLevel", $context));
    };

    AJS.$(AJS).bind("AJS.automation.form.loaded", init);
})();