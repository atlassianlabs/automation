package com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;

/**
 * @author: mmolenaar
 */
public class CustomFieldValueRetriever
{

    private final CustomFieldManager customFieldManager;
    private final Issue issue;

    public CustomFieldValueRetriever(final CustomFieldManager customFieldManager, final Issue issue)
    {
        this.customFieldManager = customFieldManager;
        this.issue = issue;
    }

    public Object get(Integer id)
    {
        CustomField customField = customFieldManager.getCustomFieldObject(id.longValue());

        if (customField == null)
        {
            return null;
        }

        return issue.getCustomFieldValue(customField);
    }

}
