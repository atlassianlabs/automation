package com.atlassian.plugin.automation.jira.util;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class ParameterParserUtil
{
    public static String getFieldsMapString(Map<String, String[]> fieldsMap)
    {
        final StringBuilder stringBuilder = new StringBuilder();
        for (String key : fieldsMap.keySet())
        {
            final String[] values = fieldsMap.get(key);
            stringBuilder.append(key).append(" = ").append(StringUtils.join(values, ",")).append("\n");
        }
        return stringBuilder.toString();
    }

    public static Map<String, String[]> getFieldsMap(final String fieldsText)
    {
        final String[] rows = fieldsText.split("\n");
        final Map<String, String[]> fields = Maps.newHashMap();
        for (final String row : rows)
        {
            if (!StringUtils.isBlank(row))
            {
                if (row.contains("="))
                {
                    String key = StringUtils.substringBefore(row, "=");
                    String value = StringUtils.substringAfter(row, "=");

                    if (StringUtils.isBlank(key))
                    {
                        return null;
                    }

                    // allow clearing of certain value by providing null
                    value=(StringUtils.isBlank(value)) ? null : value.trim();
                    fields.put(key.trim(), new String[] { value });
                }
                else
                {
                    return null;
                }
            }
        }
        return fields;
    }

}
