package com.atlassian.plugin.automation.jira.action;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.AutomationIssueEventUtil;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Implements action which publishes an event
 */
@Scanned
public class PublishEventAction implements Action<Issue>
{
    public static final String EVENT_ID_KEY = "jiraActionEventId";

    private static final String ALL_EVENTS = "jiraActionEvents";
    private static final Logger log = Logger.getLogger(PublishEventAction.class);

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final UserManager userManager;
    private final EventPublisher eventPublisher;
    private final EventTypeManager eventTypeManager;
    private Long eventId;


    @Inject
    public PublishEventAction(@ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
                              @ComponentImport final UserManager userManager,
                              @ComponentImport final EventPublisher eventPublisher,
                              @ComponentImport final EventTypeManager eventTypeManager)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userManager = userManager;
        this.eventPublisher = eventPublisher;
        this.eventTypeManager = eventTypeManager;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        eventId = Long.valueOf(singleValue(config, EVENT_ID_KEY));
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        final ApplicationUser directoryUser = userManager.getUserByName(actor);
        for (Issue issue : items)
        {
            final IssueEvent event = new IssueEvent(issue, Maps.newHashMap(), directoryUser, eventId);
            eventPublisher.publish(event);
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Published event %s", eventId));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            context.put(ALL_EVENTS, AutomationIssueEventUtil.getRenderableEvents(eventTypeManager.getEventTypes(),
                    actionConfiguration, EVENT_ID_KEY, userManager.getUserByName(actor)));
            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.publishEventAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String s)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            Long localEventId = Long.valueOf(singleValue(actionConfiguration, EVENT_ID_KEY));

            context.put(EVENT_ID_KEY, eventTypeManager.getEventType(localEventId).getName());

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.publishEventActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        if (!params.containsKey(EVENT_ID_KEY))
        {
            errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
        }
        else
        {
            for (String eventId : params.get(EVENT_ID_KEY))
            {
                if (StringUtils.isBlank(eventId) || !StringUtils.isNumeric(eventId))
                {
                    errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
                    break;
                }
            }
        }
        return errorCollection;
    }
}
