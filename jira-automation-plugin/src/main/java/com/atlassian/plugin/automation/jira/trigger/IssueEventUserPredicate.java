package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.automation.core.AutomationConfiguration;
import com.google.common.base.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;


public class IssueEventUserPredicate implements Predicate<IssueEvent>
{
    private static final Logger log = Logger.getLogger(IssueEventUserPredicate.class);
    private final CustomFieldManager customFieldManager;
    private final GroupManager groupManager;
    private final boolean restrictEventAuthors;

    private boolean currentIsAssignee;
    private boolean currentIsReporter;
    private boolean currentIsInGroup;
    private boolean enabledReporterRestrictions;
    private boolean enabledAssigneeRestrictions;
    private String userCustomFieldId;
    private String specificUser;
    private String specificGroup;

    public IssueEventUserPredicate(final AutomationConfiguration config, final CustomFieldManager customFieldManager, final GroupManager groupManager)
    {
        this.customFieldManager = customFieldManager;
        this.groupManager = groupManager;

        enabledReporterRestrictions = Boolean.parseBoolean(singleValue(config, "enabledReporterRestrictions"));
        enabledAssigneeRestrictions = Boolean.parseBoolean(singleValue(config, "enabledAssigneeRestrictions"));

        currentIsAssignee = Boolean.parseBoolean(singleValue(config, "currentIsAssignee"));
        currentIsReporter = Boolean.parseBoolean(singleValue(config, "currentIsReporter"));
        currentIsInGroup = Boolean.parseBoolean(singleValue(config, "currentIsInGroup"));

        if (currentIsAssignee && !enabledAssigneeRestrictions)
        {
            enabledAssigneeRestrictions = true;
        }

        if (currentIsReporter && !enabledReporterRestrictions)
        {
            enabledReporterRestrictions = true;
        }

        userCustomFieldId = singleValue(config, "userCustomFieldId");
        specificUser = StringUtils.trim(singleValue(config, "specificUser"));
        specificGroup = StringUtils.trim(singleValue(config, "specificGroup"));
        restrictEventAuthors = Boolean.parseBoolean(singleValue(config, "restrictEventAuthors"));
    }

    @Override
    public boolean apply(@Nullable final IssueEvent event)
    {
        if (event == null)
        {
            return false;
        }

        if (!restrictEventAuthors)
        {
            return true;
        }

        // if no conditions are set - proceed
        if (!enabledAssigneeRestrictions && !enabledReporterRestrictions
                && StringUtils.isBlank(userCustomFieldId) && StringUtils.isBlank(specificUser)
                && StringUtils.isBlank(specificGroup))
        {
            return true;
        }

        final Issue issue = event.getIssue();
        final ApplicationUser eventAuthor = getEventAuthor(event);

        // In case we have event restrictions enabled but there is no event author, we bail - we cannot determine if the event is from that user
        if (eventAuthor == null)
        {
            return false;
        }

        if (enabledReporterRestrictions && checkUser(eventAuthor, issue.getReporter(), currentIsReporter))
        {
            return true;
        }
        else
        {
            log.debug("No reporter based restriction set for user " + eventAuthor.getName() + " on issue " + issue.getKey());
        }

        if (enabledAssigneeRestrictions && checkUser(eventAuthor, issue.getAssignee(), currentIsAssignee))
        {
            return true;
        }
        else
        {
            log.debug("No assignee based restriction set for user " + eventAuthor.getName() + " on issue " + issue.getKey());
        }

        if (userCustomFieldId != null && isInCustomField(issue, eventAuthor))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not found in required custom field " + userCustomFieldId + " for issue " + issue.getKey());
        }

        if (specificUser != null && specificUser.equals(eventAuthor.getKey()))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not user " + specificUser);
        }

        if (specificGroup != null)
        {
            //Check group membership.  The user matches if in the specified group and the boolean checkIsInGroup is true
            // Or if the user is not in the group and the boolean is false (checking for group exclusion)
            if (currentIsInGroup == groupManager.isUserInGroup(eventAuthor.getName(), specificGroup))
            {
                return true;
            }
            else if (log.isDebugEnabled())
            {
                log.debug("User " + eventAuthor.getName() + " failed to meet group criteria for " + specificGroup);
            }
        }

        return false;
    }

    public String getSpecificUser()
    {
        return specificUser;
    }

    public CustomField getRestrictingCustomField()
    {
        return getRestrictingCustomField(userCustomFieldId);
    }

    private boolean checkUser(ApplicationUser eventAuthor, ApplicationUser expectedUser, Boolean eventStatus)
    {
        if (expectedUser == null)
        {
            return false;
        }

        // If any conditions are set, any one of them evaluating to true will cause us to transition.
        if (eventStatus && expectedUser.getKey().equals(eventAuthor.getKey()))
        {
            return true;
        }
        else if (!eventStatus && !expectedUser.getKey().equals(eventAuthor.getKey()))
        {
            return true;
        }

        return false;
    }

    private ApplicationUser getEventAuthor(IssueEvent event)
    {
        final ApplicationUser user = event.getUser();
        // not quite sure why this is needed. It was copied over from the toolkit auto transition listener.
        // @skarimov didn't leave a comment about why it's needed when he introduced it
        if (user == null)
        {
            if (event.getComment() != null)
            {
                return event.getComment().getAuthorApplicationUser();
            }
        }
        return user;
    }

    /**
     * @return Whether we restrict to users in a userpicker custom field.
     */
    private boolean isInCustomField(Issue issue, ApplicationUser eventAuthor)
    {
        final CustomField cf = getRestrictingCustomField(userCustomFieldId);
        if (cf == null)
        {
            log.debug("Required custom field " + userCustomFieldId + " is not found for issue " + issue.getKey());
            return false;
        }

        @SuppressWarnings("unchecked")
        final Iterable<ApplicationUser> cfUsers = (Iterable<ApplicationUser>) issue.getCustomFieldValue(cf);
        if (cfUsers == null)
        {
            log.debug("Required custom field " + userCustomFieldId + " has no values for issue " + issue.getKey());
            return false;
        }

        for (ApplicationUser cfUser : cfUsers)
        {
            if (cfUser.getKey().equals(eventAuthor.getKey()))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Parse the user's entry (if any) for a user picker custom field.
     *
     * @param customfieldId eg. "customfield_10000"
     * @return The specified custom field.
     */
    private CustomField getRestrictingCustomField(String customfieldId)
    {
        if (StringUtils.isBlank(customfieldId))
        {
            return null;
        }

        final CustomField field = customFieldManager.getCustomFieldObject(customfieldId);
        if (field != null && field.getCustomFieldType() instanceof UserField)
        {
            return field;
        }
        else
        {
            log.error("Issue Event Trigger configured to restrict to custom field with id '" +
                    customfieldId + "', but no such custom field was found. Value expected to be customfield_<id>, eg. customfield_10000.");
            return null;
        }
    }
}
