package com.atlassian.plugin.automation.jira.action;

import com.atlassian.plugin.automation.jira.util.ParameterParserUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Map;

public class TestParameterParserUtil extends TestCase
{
    @Test
    public void testParseSuccess()
    {
        String params = "key1=value\nkey2=other value";
        Map<String, String[]> result = ParameterParserUtil.getFieldsMap(params);

        assertEquals(result.get("key1").length, 1);
        assertEquals(result.get("key1")[0], "value");
        assertEquals(result.get("key2").length, 1);
        assertEquals(result.get("key2")[0], "other value");

        result = ParameterParserUtil.getFieldsMap("plop=");
        assertEquals(1, result.get("plop").length);
        assertEquals(null, result.get("plop")[0]);
    }

    @Test
    public void testParseFail()
    {
        assertNull(ParameterParserUtil.getFieldsMap("plop"));

        assertNull(ParameterParserUtil.getFieldsMap("=plop"));

        assertNull(ParameterParserUtil.getFieldsMap("="));

        assertNull(ParameterParserUtil.getFieldsMap("plop=plouf\nplip"));
    }

}
