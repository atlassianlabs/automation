package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.jql.query.LuceneQueryBuilder;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.jira.util.JqlMatcherService;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssuePropertySetEventTrigger
{
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private LuceneQueryBuilder luceneQueryBuilder;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private JqlMatcherService jqlMatcherService;

    @Before
    public void setUp() throws Exception
    {
        ComponentAccessor.Worker worker = Mockito.mock(ComponentAccessor.Worker.class);
        ComponentAccessor.initialiseWorker(worker);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(Constants.JIRA_62_BUILD_NUMBER);

        when(ComponentAccessor.getComponentOfType(LuceneQueryBuilder.class)).thenReturn(luceneQueryBuilder);
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        final IssuePropertySetEventTrigger issuePropertySetEventTrigger = new IssuePropertySetEventTrigger(soyTemplateRenderer, buildUtilsInfo, jqlMatcherService);

        final Map<String, List<String>> params = Maps.newHashMap();
        assertTrue("errors present: event property key(s) required", issuePropertySetEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssuePropertySetEventTrigger.EVENT_PROPERTY_KEYS, newArrayList(""));
        assertTrue("errors present: event property key(s) required", issuePropertySetEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssuePropertySetEventTrigger.EVENT_PROPERTY_KEYS, newArrayList("testsingleprop"));
        assertFalse("no errors present", issuePropertySetEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssuePropertySetEventTrigger.EVENT_PROPERTY_KEYS, newArrayList("testoneprop,testtwoprop"));
        assertFalse("no errors present", issuePropertySetEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }

}
